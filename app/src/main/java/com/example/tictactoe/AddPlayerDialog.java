package com.example.tictactoe;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import java.util.Locale;

public class AddPlayerDialog extends DialogFragment {

    private static final String ARG_PLAYER_POS = "ARG_PLAYER_POS";
    private static final String ARG_MARK_TAKEN = "ARG_MARK_TAKEN";

    private AddPlayerDialogListener mListener;
    private Mark mChosenMark = Mark.X; // Defaulting to X in the event they just click ready

    @BindView(R.id.input_name)
    EditText mNameInput;
    @BindView(R.id.image_o)
    ImageView mImageO;
    @BindView(R.id.image_x)
    ImageView mImageX;
    @BindView(R.id.label_or)
    TextView mLabelOr;

    /**
     * Static action to hide views
     */
    static final ButterKnife.Action<View> HIDE = (view, index) -> view.setVisibility(View.GONE);

    /**
     * Interface for passing back {@link Player}
     */
    public interface AddPlayerDialogListener {
        void onPlayerAdded(Player player);
    }

    /**
     * Static initializer for {@link AddPlayerDialog}.
     *
     * @param playerPos Player position, e.g. Player _1_
     * @param markTaken The {@link Mark} that has been chosen already
     * @return returns instance of {@link AddPlayerDialog}
     */
    public static AddPlayerDialog getInstance(int playerPos, Mark markTaken) {
        AddPlayerDialog dialog = new AddPlayerDialog();

        // Bundle args
        Bundle args = new Bundle();
        args.putInt(ARG_PLAYER_POS, playerPos);

        // Bundle mark if present
        if (markTaken != null) {
            args.putSerializable(ARG_MARK_TAKEN, markTaken);
        }

        dialog.setArguments(args);
        return dialog;
    }

    /**
     * Static initializer for {@link AddPlayerDialog}.
     *
     * @param playerPos Player position, e.g. Player _1_
     * @return returns instance of {@link AddPlayerDialog}
     */
    public static AddPlayerDialog getInstance(int playerPos) {
        return getInstance(playerPos, null);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_add_player, null);
        ButterKnife.bind(this, view);

        // Get Args
        Bundle arguments = getArguments();
        int playerPos = arguments.getInt(ARG_PLAYER_POS, 1);

        // If a mark is taken, only one choice
        if (arguments.containsKey(ARG_MARK_TAKEN)) {
            Mark taken = (Mark) arguments.getSerializable(ARG_MARK_TAKEN);
            onOneOption(view, taken);
        }

        // Build Dialog
        builder.setView(view)
                .setPositiveButton(R.string.button_ready, this::onReady)
                .setTitle(formatTitle(playerPos));

        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * Handle when there is only one {@link Mark} to choose from.
     *
     * @param view  parent view
     * @param taken {@link Mark} that's been chosen
     */
    private void onOneOption(View view, Mark taken) {
        // Hide OR
        ButterKnife.apply(mLabelOr, HIDE);

        // Hide already chosen mark
        if (taken == Mark.O) {
            ButterKnife.apply(mImageO, HIDE);
            mChosenMark = Mark.X;
        } else {
            ButterKnife.apply(mImageX, HIDE);
            mChosenMark = Mark.O;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Check that parent implements listener
        try {
            mListener = (AddPlayerDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement NoticeDialogListener");
        }
    }

    /**
     * Click handler for positive button.
     *
     * @param dialog calling dialog
     * @param which  id of pressed button
     */
    private void onReady(DialogInterface dialog, int which) {
        // Build Player
        String name = mNameInput.getText().toString();
        mListener.onPlayerAdded(new Player(name, mChosenMark));

        // Close
        dismiss();
    }

    /**
     * Given player position, returns formatted title.
     *
     * @param playerPos Player position, e.g. Player _1_
     * @return formatted title
     */
    private String formatTitle(int playerPos) {
        return String.format(Locale.getDefault(), getString(R.string.title_player_x), playerPos);
    }

    /**
     * Listen to image clicks
     *
     * @param view calling view
     */
    @OnClick({R.id.image_x, R.id.image_o})
    void onMarkChosen(View view) {
        // Save & highlight choice
        if (view.getId() == R.id.image_o) {
            mChosenMark = Mark.O;
            ButterKnife.apply(mImageO, View.ALPHA, 1f);
            ButterKnife.apply(mImageX, View.ALPHA, 0.1f);
        } else {
            mChosenMark = Mark.X;
            ButterKnife.apply(mImageO, View.ALPHA, 0.1f);
            ButterKnife.apply(mImageX, View.ALPHA, 1f);
        }
    }

}
