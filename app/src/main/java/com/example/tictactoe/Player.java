package com.example.tictactoe;

/**
 * Represents a player of the game
 */
public class Player {
    private String name;
    private Mark mark;

    /**
     * Constructor for creating a Player
     *
     * @param name of the player
     * @param mark to represent the player's moves, see {@link Mark}
     */
    public Player(String name, Mark mark) {
        this.name = name;
        this.mark = mark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Mark getMark() {
        return mark;
    }

    public void setMark(Mark mark) {
        this.mark = mark;
    }
}
