package com.example.tictactoe;

import android.graphics.Point;
import android.util.Log;

/**
 * Helper class to map view cells to {@link Point}s (e.g. coordinate) on the {@link Board}.
 * <p>
 * Bit of a code smell, but it works!
 */
public class BoardMapper {
    private static final String TAG = BoardMapper.class.getSimpleName();

    /**
     * Converts cell view to corresponding coordinates in the {@link Board}
     *
     * @param cellID resource ID of the cell view
     * @return Point, containing the X & Y coordinates
     */
    static Point viewToPoint(int cellID) {
        switch (cellID) {
            case R.id.cell_A1:
                return new Point(0, 0);
            case R.id.cell_A2:
                return new Point(1, 0);
            case R.id.cell_A3:
                return new Point(2, 0);
            case R.id.cell_B1:
                return new Point(0, 1);
            case R.id.cell_B2:
                return new Point(1, 1);
            case R.id.cell_B3:
                return new Point(2, 1);
            case R.id.cell_C1:
                return new Point(0, 2);
            case R.id.cell_C2:
                return new Point(1, 2);
            case R.id.cell_C3:
                return new Point(2, 2);
            default:
                ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException = new ArrayIndexOutOfBoundsException();
                Log.e(TAG, "viewToPoint: That's not supposed to happen!", arrayIndexOutOfBoundsException);
                throw arrayIndexOutOfBoundsException;
        }
    }

    /**
     * Converts coordinates on the {@link Board} to the corresponding cell view
     *
     * @param point coordinates on the Board
     * @return resource ID of the cell view
     */
    static int pointToView(Point point) {
        if (point.equals(0, 0)) {
            return R.id.cell_A1;
        } else if (point.equals(1, 0)) {
            return R.id.cell_A2;
        } else if (point.equals(2, 0)) {
            return R.id.cell_A3;
        } else if (point.equals(0, 1)) {
            return R.id.cell_B1;
        } else if (point.equals(1, 1)) {
            return R.id.cell_B2;
        } else if (point.equals(2, 1)) {
            return R.id.cell_B3;
        } else if (point.equals(0, 2)) {
            return R.id.cell_C1;
        } else if (point.equals(1, 2)) {
            return R.id.cell_C2;
        } else if (point.equals(2, 2)) {
            return R.id.cell_C3;
        } else {
            ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException = new ArrayIndexOutOfBoundsException();
            Log.e(TAG, "pointToView: That's not supposed to happen!", arrayIndexOutOfBoundsException);
            throw arrayIndexOutOfBoundsException;
        }
    }
}
