package com.example.tictactoe;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

import java.util.List;

import static com.example.tictactoe.Scenario.*;

public class Game {

    private static final String TAG = Game.class.getSimpleName();
    private List<Player> mPlayers;
    private Player mCurrentPlayer;
    private Board mBoard;
    private PublishSubject<Update> mBoardUpdates;

    Game(List<Player> players) {
        mPlayers = players;
        mCurrentPlayer = players.get(0); // Start with Player 1
        mBoard = new Board();
        mBoardUpdates = PublishSubject.create();
    }

    Observable<Update> getGameUpdates() {
        return mBoardUpdates;
    }

    void selectSquare(int x, int y) {
        System.out.println(String.format("%1s: selectSquare: {x: %2d, y: %3d}", TAG, x, y));
        boolean success = mBoard.markCell(mCurrentPlayer.getMark(), x, y);

        // Exit if unsuccessful
        if (!success) {
            return;
        }

        // Update
        mBoardUpdates.onNext(new Update(mCurrentPlayer, x, y, checkForDraw(), checkForWinner()));
        nextTurn();
    }

    private void nextTurn() {
        mCurrentPlayer = mCurrentPlayer.equals(mPlayers.get(0)) ? mPlayers.get(1) : mPlayers.get(0);
    }

    private boolean checkForWinner() {
        Mark[][] cells = mBoard.getCells();

        // Check X & Y for 3 in a row
        for (int i = 0; i <= 2; i++) {
            if (threeHorizontal(cells, i)) {
                return true;
            }

            if (threeVertical(cells, i)) {
                return true;
            }
        }

        //Check down diagonal [\] for 3 in a row
        if (threeDiagonalDown(cells)) {
            return true;
        }

        // Check up diagonal [/] for 3 in a row
        if (threeDiagonalUp(cells)) {
            return true;
        }

        // Otherwise
        return false;
    }

    private boolean checkForDraw() {
        Mark[][] cells = mBoard.getCells();
        boolean foundEmpty = false;

        // Scan for Empty, exiting at first one.
        loop:
        for (int x = 0; x <= 2; x++) {
            for (int y = 0; y <= 2; y++) {
                if (cells[y][x] == Mark.EMPTY) {
                    foundEmpty = true;
                    break loop;
                }
            }
        }

        // True if there were no Empty's found.
        return !foundEmpty;
    }

}
