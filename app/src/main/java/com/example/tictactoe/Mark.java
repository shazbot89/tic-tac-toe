package com.example.tictactoe;

/**
 * Represents the available Marks for Tic Tac Toe
 */
public enum Mark {
    X(R.drawable.ic_x),
    O(R.drawable.ic_o),
    EMPTY(0);

    private final int id;

    /**
     * Constructor for creating a Mark
     *
     * @param id ID of the drawable that corresponds to the Mark
     */
    Mark(int id) {
        this.id = id;
    }

    /**
     * Resource drawable that corresponds to the Mark
     *
     * @return resource ID
     */
    public int getValue() {
        return id;
    }
}
