package com.example.tictactoe;

import android.graphics.Point;

/**
 * Represents updates to the game
 */
class Update {
    private Player player;
    private Point move;
    private boolean isDraw;
    private boolean isWinner;

    /**
     * Constructor for creating an Update
     *
     * @param player   which player this update is about
     * @param x        x coordinate of their move on the {@link Board}
     * @param y        y coordinate of their move on the {@link Board}
     * @param isDraw   return true if the game has ended in a draw
     * @param isWinner return true if the game has been won
     */
    Update(Player player, int x, int y, boolean isDraw, boolean isWinner) {
        this.player = player;
        this.move = new Point(x, y);
        this.isDraw = isDraw;
        this.isWinner = isWinner;
    }

    public Player getPlayer() {
        return player;
    }

    public Point getMove() {
        return move;
    }

    public boolean isDraw() {
        return isDraw;
    }

    public boolean isWinner() {
        return isWinner;
    }
}
