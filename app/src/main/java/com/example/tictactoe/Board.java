package com.example.tictactoe;

import java.util.Arrays;

/**
 * Represents the board of the game, a two-dimensional 3x3 grid.
 */
public class Board {
    private static final String TAG = Board.class.getSimpleName();
    private Mark[][] mCells;

    /**
     * Constructor, initializing a 3x3 board of empty {@link Mark}s
     */
    Board() {
        mCells = new Mark[][]{
                {Mark.EMPTY, Mark.EMPTY, Mark.EMPTY},
                {Mark.EMPTY, Mark.EMPTY, Mark.EMPTY},
                {Mark.EMPTY, Mark.EMPTY, Mark.EMPTY},
        };
    }

    /**
     * Places a mark at the provided X & Y coordinates
     *
     * @param mark  {@link Mark} to place
     * @param cellX x coordinate on the board
     * @param cellY y coordinate on the baord
     * @return true, if the space is Empty; false if it's already taken.
     */
    boolean markCell(Mark mark, int cellX, int cellY) {
        // Check if cell is empty, return false if not
        if (mCells[cellY][cellX] != Mark.EMPTY) {
            return false;
        }

        try {
            mCells[cellY][cellX] = mark;
        } catch (IndexOutOfBoundsException e) {
            System.out.println(String.format("%1s: Cell {x: %2d, y: %3d} is out of bounds!", TAG, cellX, cellY));
        }

        System.out.println(String.format("%1s:  markCell: %2s", TAG, Arrays.deepToString(mCells)));
        return true;
    }

    public Mark[][] getCells() {
        return mCells;
    }
}
