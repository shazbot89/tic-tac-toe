package com.example.tictactoe;

/**
 * Class of scenarios / outcomes of the game to check for
 */
public class Scenario {

    /**
     * Check if three in a row match, horizontally
     *
     * @param cells 2D array of cells
     * @param i     index for the Y position
     * @return true if scenario met, false otherwise
     */
    public static boolean threeHorizontal(Mark[][] cells, int i) {
        return (cells[i][0] == cells[i][1]) && (cells[i][1] == cells[i][2]) // Row matches
                && (cells[i][0] != Mark.EMPTY); // But isn't Empty
    }

    /**
     * Check if three in a row match, vertically
     *
     * @param cells 2D array of cells
     * @param i     index for the Y position
     * @return true if scenario met, false otherwise
     */
    public static boolean threeVertical(Mark[][] cells, int i) {
        return (cells[0][i] == cells[1][i]) && (cells[1][i] == cells[2][i]) // Column matches
                && (cells[0][i] != Mark.EMPTY); // But isn't Empty;
    }

    /**
     * Check if three in a row match, diagonally up [ / ]
     *
     * @param cells 2D array of cells
     * @return true if scenario met, false otherwise
     */
    public static boolean threeDiagonalUp(Mark[][] cells) {
        return (cells[0][2] == cells[1][1]) && (cells[1][1] == cells[2][0]) // Diagonal matches
                && (cells[0][2] != Mark.EMPTY); // But isn't Empty;
    }

    /**
     * Check if three in a row match, diagonally down [ \ ]
     *
     * @param cells 2D array of cells
     * @return true if scenario met, false otherwise
     */
    public static boolean threeDiagonalDown(Mark[][] cells) {
        return (cells[0][0] == cells[1][1]) && (cells[1][1] == cells[2][2]) // Diagonal matches
                && (cells[0][0] != Mark.EMPTY); // But isn't Empty;
    }
}
