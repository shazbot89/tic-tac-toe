package com.example.tictactoe;

import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.Disposable;

import java.util.ArrayList;
import java.util.List;

/**
 * Hosts the UI and interaction for players with the {@link Game}.
 */
public class MainActivity extends AppCompatActivity implements AddPlayerDialog.AddPlayerDialogListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private Game mGame;
    private Disposable mGameUpdates;
    private List<Player> mPlayers;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.name_player_1)
    TextView mPlayerOne;
    @BindView(R.id.name_player_2)
    TextView mPlayerTwo;
    @BindViews({R.id.cell_A1, R.id.cell_A2, R.id.cell_A3, R.id.cell_B1, R.id.cell_B2, R.id.cell_B3, R.id.cell_C1,
            R.id.cell_C2, R.id.cell_C3})
    List<ImageView> mBoardViews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);

        newGame();
    }

    @Override
    protected void onDestroy() {
        mGameUpdates.dispose();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_credits) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.title_credits)
                    .setMessage(R.string.message_credits)
                    .setPositiveButton(R.string.ok, null)
                    .show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Handle click events of the {@link FloatingActionButton}
     *
     * @param view view that sent the event
     */
    @OnClick(R.id.fab)
    void onFabClicked(View view) {
        Snackbar.make(view, "Start a New Game?", Snackbar.LENGTH_LONG)
                .setAction("Yes", v -> newGame()).show();
    }

    /**
     * Resets the game state, prompting for new players
     */
    private void newGame() {
        // Clear players
        mPlayers = new ArrayList<>();
        // Reset board
        ButterKnife.apply(mBoardViews, (view, index) -> view.setImageDrawable(null));
        ButterKnife.apply(mBoardViews, (view, index) -> view.setClickable(true));
        // Get players
        getPlayer(1);
    }

    /**
     * Prompts for player details
     *
     * @param player position for the player, e.g. Player 1, Player 2
     */
    private void getPlayer(int player) {
        AddPlayerDialog dialog;

        if (player == 1) {
            dialog = AddPlayerDialog.getInstance(player);
        } else {
            Mark taken = mPlayers.get(0).getMark();
            dialog = AddPlayerDialog.getInstance(player, taken);
        }

        dialog.show(getSupportFragmentManager(), "PlayerDialog");
    }

    /**
     * Begin a new game
     */
    private void startGame() {
        mGame = new Game(mPlayers);
        mGameUpdates = mGame.getGameUpdates().subscribe(this::onNext);
        showPlayers();
    }

    /**
     * Presents the player and their color to the UI
     */
    private void showPlayers() {
        // Get players
        Player player1 = mPlayers.get(0);
        Player player2 = mPlayers.get(1);

        // Set Names
        mPlayerOne.setText(player1.getName());
        mPlayerTwo.setText(player2.getName());

        // Resolve Colors (Using deprecated method as min SDK is API 21)
        int x = getResources().getColor(R.color.colorX);
        int o = getResources().getColor(R.color.colorO);

        // Set player name to color
        mPlayerOne.setTextColor(player1.getMark() == Mark.X ? x : o);
        mPlayerTwo.setTextColor(player2.getMark() == Mark.X ? x : o);
    }

    /**
     * Handles OnClick events for the cells of the board
     *
     * @param view view that was tapped
     */
    @OnClick({R.id.cell_A1, R.id.cell_A2, R.id.cell_A3,
            R.id.cell_B1, R.id.cell_B2, R.id.cell_B3,
            R.id.cell_C1, R.id.cell_C2, R.id.cell_C3})
    void onBoardTapped(View view) {
        try {
            Point point = BoardMapper.viewToPoint(view.getId());
            mGame.selectSquare(point.x, point.y);
        } catch (Exception e) {
            Toast.makeText(this, "Invalid Move!", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receives updates from the game, checking for end {@link Scenario}s and updating the board
     *
     * @param update the update from the game
     */
    private void onNext(Update update) {
        checkForGameOver(update);
        // Update Board
        Point move = update.getMove();
        Mark mark = update.getPlayer().getMark();
        updateBoardView(move, mark);
    }

    /**
     * Checks for and responds accordingly to game ending {@link Update}s
     *
     * @param update the update from the game
     */
    private void checkForGameOver(Update update) {
        // Check for winning move
        if (update.isWinner()) {
            // Disable board on win
            ButterKnife.apply(mBoardViews, (view, index) -> view.setClickable(false));
            // Celebrate!
            showWinningDialog(update.getPlayer());
            return;
        }

        // Check for draw move
        if (update.isDraw()) {
            // TODO Dialog
            Toast.makeText(this, "We have a DRAW!!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Updates the cells of the board after players have selected a cell
     *
     * @param point X, Y coordinate on the board
     * @param mark  player mark to mark the cell with
     */
    private void updateBoardView(Point point, Mark mark) {
        Log.d(TAG, String.format("updateBoardView: Move: %1s, Mark: %2s", point, mark));
        int cellID = BoardMapper.pointToView(point);
        ((ImageView) findViewById(cellID)).setImageResource(mark.getValue());
    }

    /**
     * Loops until sufficient number of  players are added, starting once there are.
     *
     * @param player the added player
     */
    @Override
    public void onPlayerAdded(Player player) {
        mPlayers.add(player);

        // Ask if we don't have two players
        if (mPlayers.size() < 2) {
            getPlayer(2);
        } else {
            startGame();
        }
    }

    private void showWinningDialog(Player player) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.title_winner)
                .setIcon(player.getMark().getValue())
                .setView(R.layout.dialog_win)
                .setMessage(String.format(getString(R.string.message_win), player.getName()))
                .setPositiveButton(R.string.woohoo, null)
                .show();
    }
}
