package com.example.tictactoe;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class BoardTest {

    @Test
    public void markCell() {
        Mark[][] expected = {
                {Mark.X, Mark.EMPTY, Mark.EMPTY},
                {Mark.EMPTY, Mark.EMPTY, Mark.O},
                {Mark.EMPTY, Mark.EMPTY, Mark.EMPTY},
        };

        // Mark cells on board
        Board board = new Board();
        board.markCell(Mark.X, 0, 0);
        board.markCell(Mark.O, 2, 1);

        assertArrayEquals(expected, board.getCells());
    }

    @Test
    public void getCells() {
        Mark[][] expected = {
                {Mark.EMPTY, Mark.EMPTY, Mark.EMPTY},
                {Mark.EMPTY, Mark.EMPTY, Mark.EMPTY},
                {Mark.EMPTY, Mark.EMPTY, Mark.EMPTY},
        };
        Mark[][] actual = new Board().getCells();
        assertArrayEquals(expected, actual);
    }
}