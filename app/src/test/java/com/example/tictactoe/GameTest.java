package com.example.tictactoe;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class GameTest {

    private Game game;

    @Before
    public void setUp() throws Exception {
        List<Player> players = new ArrayList<>();
        players.add(new Player("TestUser", Mark.X));
        players.add(new Player("AWildCompetitorAppeared", Mark.O));
        game = new Game(players);
    }

    @Test
    public void selectSquare() {
        game.selectSquare(1, 0);

        game.getGameUpdates().subscribe((update) -> {
            // Mark
            assertEquals(1, update.getMove().x);
            assertEquals(0, update.getMove().y);
            // De-false
            assertFalse(update.isDraw());
            assertFalse(update.isWinner());
        });
    }

}