# Tic Tac Toe
A sample Tic Tac Toe application in Android.

### Goals
*  Allow 2 players to play tic tac toe (Follow the rules of tic tac toe)
*  Have 3x3 grid on which the players can play
*  Allow the players to take turns marking spaces on the 3x3 grid
*  Recognize when a player has won and declare that player as victorious
*  Allow the user to start a new game

### Credits
* Icons made by [Freepik](https://www.flaticon.com/authors/freepik) from www.flaticon.com

### Libraries
* RxJava - https://github.com/ReactiveX/RxJava
* ButterKnife - http://jakewharton.github.io/butterknife/